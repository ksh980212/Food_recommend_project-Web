import React, { useState } from 'react';
import '../style/App.css';
import ResetStyles from './config/ResetStyle';
import TopNavbar from './navigator/TopNavbar';
import BottomNavbar from './navigator/BottomNavbar';
import Developer from './info/Developer';
import SignupComplete from './auth/SignupComplete';
import About from './info/About';
import Recommend_home from './api/Cuisine_search_home';
import Home from './info/Home';
import FindUserId from './auth/FindUserId';
import FindUserPw from './auth/FindUserPw';
import { Route, Switch, Link } from 'react-router-dom';
import Signup from './auth/Signup';
import Login from './auth/Login';
import Recipe from './api/Cuisine_recipe';
import Cuisine_choice from './api/Cuisine_choice';
import { Button } from 'reactstrap';
import Evaulation from './info/Evaluation';
import Cuisine_recommend_home from './api/Cuisine_recommend_home';
import Privacy from './auth/Privacy';
import instagramRecommend from './api/Instagram_recommend';
import Filter_choice from './api/Filter_choice';


const App = () => {
  const buttonstyle = {
    marginTop: '20px',
    color: 'white',
  };

  const [isLogined, setIsLogined] = useState(localStorage.getItem('token'));
  /**로그인 여부 상태 */

  const changeLogoutState = () => {
    setIsLogined(false);
  };
  /** 로그인 => 로그아웃 상태변경 */

  const changeLoginState = () => {
    setIsLogined(true);
  };
  /** 로그아웃=> 로그인 상태변경 */

  return (
    <React.Fragment>
      <ResetStyles />
      <header>
        <TopNavbar isLogined={isLogined} changeLogoutState={changeLogoutState}/>
      </header>

      <section className="App-section-layout">
        <Switch>
          <Route exact path="/" component={()=><Home isLogined={isLogined} />} />
          <Route path="/signup" component={Signup} />
          <Route path="/login" component={() => <Login changeLoginState={changeLoginState} />}/>
          <Route path="/findUserId" component={FindUserId} />
          <Route path="/findUserPw" component={FindUserPw} />
          <Route path="/developer" component={Developer} />
          <Route path="/about" component={About} />
          <Route path="/search_cuisine" component={Recommend_home} />
          <Route path="/recommend_home" component={Cuisine_recommend_home} />
          <Route path="/signupComplete" component={SignupComplete} />
          <Route path="/recipe" component={() => <Recipe isLogined={isLogined} />}/>
          <Route path="/worldcup" component={Cuisine_choice} />
          <Route path="/evaluation" component={Evaulation} />
          <Route path="/privacy" component={Privacy} />
          <Route path="/instagramRecommend" component={instagramRecommend} />
          <Route path ="/filterChoice" component={Filter_choice} />
          <Route render={(_) => (
              <div className="App-section-layout2">
                <h2>이 페이지는 존재하지 않습니다.</h2><hr />
                <Link to="/">
                  <Button style={buttonstyle}>홈으로 이동</Button>
                </Link>
              </div> )}/>
        </Switch>
      </section>

      <footer>
        <BottomNavbar />
      </footer>
    </React.Fragment>
  );
};
export default App;
