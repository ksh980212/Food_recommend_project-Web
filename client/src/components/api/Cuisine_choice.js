import React from 'react'
import { useHistory} from 'react-router-dom';
import meat from '../../images/meat.PNG';
import noodle from '../../images/noodle.PNG';
import boonsik from '../../images/boonsik.PNG';
import west from '../../images/west.PNG';
import china from '../../images/china.PNG'
import fast from '../../images/fast.PNG'
import korea from '../../images/korea.PNG'
import japan from '../../images/japan.PNG'
import bread from '../../images/bread.PNG';


const food=[
    {key:0, value:['치킨','버거','강정','감자튀김']},
    {key:1, value:['비빔국수','냉면','라면','칼국수']},
    {key:2, value:['짬뽕','볶음밥','탕수육','칼국수']},
    {key:3, value:['피자','파스타','샐러드','스프']},
    {key:4, value:['초밥','우동','돈까스','카레']},
    {key:5, value:['스테이크','불고기','닭갈비','연어']},
    {key:6, value:['떡볶이','튀김','김밥','만두']},
    {key:7, value:['비빔밥','된장찌개','김치찌개','콩나물국']},
    {key:8, value:['식빵','스콘','샌드위치','토스트']},
  ]
  

const Cuisine_choice = () => {
    
    const history = useHistory();
    let temp=[]; //인덱스
    let include=[]; //이름

    const imgBtnClick= (e)=>{

        if(temp.includes(e.target.alt)){
            
            for(let i=0; i<temp.length; i++){
                if(temp[i]=== e.target.alt){
                    temp.splice(i,1);
                }
            }
            e.target.style.opacity=1;
        }
        else{
            temp.push(e.target.alt);
            e.target.style.opacity=0.2;
        }

        if(temp.length===3){
            submitBtnClick();
        }
    }

    const submitBtnClick= (e)=>{
        for(let i=0; i<temp.length; i++){
            include.push(food[temp[i]].value);
        }   
        history.push({
            pathname:'/filterChoice',
            state:{include:include}
        })
    }
    

    return (
      <section className="jumbotron text-center" id="filter">
        <p className="section-title">당신이 좋아하는</p>
        <p>3가지 카테고리를 선택해주세요</p>
        <div className="row">
            <div className="col-md-4" id="col-md-4-id">
                <div  className="card-body" id="card-body-id">
                    <img onClick={imgBtnClick} src={korea} className="Cuisine-choice-img" alt="7"/>
                </div>
            </div>
            <div className="col-md-4" id="col-md-4-id">
                <div  className="card-body" id="card-body-id">
                    <img onClick={imgBtnClick} src={meat} className="Cuisine-choice-img" alt="5"/>
                </div>
            </div>
            <div className="col-md-4" id="col-md-4-id">
                <div  className="card-body" id="card-body-id">
                    <img onClick={imgBtnClick} src={fast} className="Cuisine-choice-img" alt="0"/>
                </div>
            </div>
            <div className="col-md-4" id="col-md-4-id">
                <div  className="card-body" id="card-body-id">
                    <img onClick={imgBtnClick} src={noodle} className="Cuisine-choice-img" alt="1"/>
                </div>
            </div>
            <div className="col-md-4" id="col-md-4-id">
                <div  className="card-body" id="card-body-id">
                    <img onClick={imgBtnClick} src={china} className="Cuisine-choice-img" alt="2"/>
                </div>
            </div>
            <div className="col-md-4" id="col-md-4-id">
                <div  className="card-body" id="card-body-id">
                    <img onClick={imgBtnClick} src={west} className="Cuisine-choice-img" alt="3"/>
                </div>
            </div>
            <div className="col-md-4" id="col-md-4-id">
                <div  className="card-body" id="card-body-id">
                    <img onClick={imgBtnClick} src={japan} className="Cuisine-choice-img" alt="4"/>
                </div>
            </div>
            <div className="col-md-4" id="col-md-4-id">
                <div  className="card-body" id="card-body-id">
                    <img onClick={imgBtnClick} src={boonsik} className="Cuisine-choice-img" alt="6"/>
                </div>
            </div>

            <div className="col-md-4" id="col-md-4-id">
                <div  className="card-body" id="card-body-id">
                    <img onClick={imgBtnClick} src={bread} className="Cuisine-choice-img" alt="8"/>
                </div>
            </div> 
            
        </div>

      </section>
    )
}

export default Cuisine_choice;
