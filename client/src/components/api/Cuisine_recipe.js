import React, { useState, useEffect } from 'react';
import '../../style/Api.css';
import { Card,CardImg,CardBody,CardTitle,Table,Alert,  Progress } from 'reactstrap';
import axios from 'axios';
const {REACT_APP_IP} = process.env;

const Receipt = ({ isLogined }) => {
  const [name, setName] = useState('없음');
  const [image, setImage] = useState('');
  const [people, setPeople] = useState('0인분');
  const [time, setTime] = useState('0분');
  const [eachProcedure, setEachProcedure] = useState(['없음']);
  const [eachIngredient, setEachIngredient] = useState(['없음:없음']);

  const [review, setReview] = useState(0);
  const [count, setCount] = useState(0);

  const food_id = window.location.search.substr( 6, window.location.search.length);

  const [user_id]=useState(localStorage.getItem('user_id'));

  const [star, setStar] = useState(3);

  const rangeChange = e => {
    setStar(e.target.value);
  };

  const reviewButtonClicked = e => {
    if (!isLogined) {
      alert('로그인 후 이용가능합니다');
      return;
    }

    alert('평가 감사합니다');
    e.target.disabled = 'true';

    axios.post(`http://${REACT_APP_IP}:8000/api/v1/food/food_evaluation`, { food_evaluation_score: Number(star), food_id: food_id, user_id:user_id })
      .then()
      .catch(() => {});
  };

  let ingredient_count = 1;
  let procedure_count = 1;


  useEffect(() => {
    axios
      .get(`http://${REACT_APP_IP}:8000/api/v1/food/food_detail?id=${food_id}`)
      .then(response => {
        let raw_ingredient = '';
        let raw_procedure = '';
        const setting = response.data;

        setName(response.data[0].name);
        setImage(response.data[0].image);

        for (let i = 0; i < setting.length; i++) {
          const food_key = setting[i].food_key;
          const food_value = setting[i].food_value;

          switch (food_key) {
            case 'people':
              setPeople(food_value);
              break;
            case 'time':
              setTime(food_value);
              break;
            case 'procedure':
              raw_procedure = food_value;
              break;
            case 'ingredient':
              raw_ingredient = food_value;
              break;
            default:
              break;
          }
        }

        const ingredient_temp = raw_ingredient.split('=');
        let temp = [];
        for (let i = 0; i < ingredient_temp.length; i++) {
          temp[i] = ingredient_temp[i];
        }
        setEachIngredient(temp);

        let procedure_temp = raw_procedure.split('@');
        temp = [];
        for (let i = 0; i < procedure_temp.length; i++) {
          temp[i] = procedure_temp[i];
        }
        setEachProcedure(temp);
      })
      .catch(() => {});


    axios.get(`http://${REACT_APP_IP}:8000/api/v1/food/food_review/detail?food_id=${food_id}`)
      .then(response => {
        if (response.data[0].count === 0) {
          return;
        }
        setReview(Number(response.data[0][0].average).toFixed(1));
        setCount(response.data[0][0].count);
      })
      .catch(() => {});

  }, [food_id]);

  return (
    <div className="App-section-layout">
      <Card className="Cuisine-recipe-card">
      <div className="p-4 p-md-5  rounded">
        <CardImg top src={image || 'http://placehold.it/320x320'} id="recipe-img" alt={name || 'none'}/>
      </div>
        <CardBody>
          <hr />

          <CardTitle>
            <h3 className="black">{`${name} [${people}]`}</h3>
          </CardTitle>

          <Table striped>
            <thead>
              <tr>
                <th>#</th>
                <th>재료</th>
                <th>비고</th>
              </tr>
            </thead>

            <tbody>
              {eachIngredient.map(each => {
                return (
                  <React.Fragment key={ingredient_count}>
                    <tr>
                      <td>{ingredient_count++}</td>
                      <td id="td-ingredient">{each}</td>
                      <td></td>
                    </tr>
                  </React.Fragment>
                );
              })}
            </tbody>
          </Table>
          <hr />

          <h4 className="black">조리 순서</h4>
          {eachProcedure.map(each => {
            return (
              <React.Fragment key={procedure_count++}>
                <Alert id="procedure" className="left" color="primary">{each}</Alert>
              </React.Fragment>
            );
          })}

          <h4 className="black">조리 시간</h4>
          <Alert color="success">{time}</Alert>

        </CardBody>
      </Card>

      <Card className="Cuisine-recipe-show-review">
        <h3 className="black">평가보기</h3>
        <div>{review}/5점</div>
        <Progress value={review * 20} />

        <div>
          <h5>현재까지 {count}명의 사람이 평가하였습니다.</h5>
        </div>
      </Card>

      <Card className="Cuisine-recipe-create-review">
        <h3 className="black">평가하기</h3>
        <div>
          <input onChange={rangeChange} type="range" max="5" min="1" value={star}></input>
        </div>
        <p>{star}점/5점</p>
        <button className="Cuisine-recipe-review-btn" onClick={reviewButtonClicked} >평가하기</button>
      </Card>
    </div>
  );
};

export default Receipt;
