import React, { useState, useEffect} from 'react';
import { Spinner} from 'reactstrap';
import '../../style/Api.css';
import axios from 'axios';
import {Link, useHistory} from 'react-router-dom';
const {REACT_APP_IP} = process.env;


const Cuisine_recommend_home = (props) => {
  const [isLoading, setIsLoading] = useState(true);
  const [food_data, setFood_date] = useState([]);
  const history= useHistory();

  const include =props.location.state.include
  const exclude= props.location.state.exclude;

  useEffect(()=>{

    axios.post(`http://${REACT_APP_IP}:8000/api/v1/food/food_recommend_home`,{
        include:include,
        exclude:exclude
      }).then((response)=>{
        setFood_date(response.data);
        setTimeout(() => {
          setIsLoading(false);
        }, 1000);
        
      }).catch(()=>{
        alert("서버가 불안정합니다. 잠시후 다시 시도 해주세요");
        history.push('/');
      })


  },[include, exclude, history]);

  const food_list= food_data.map(food => {
    return (
      <div className="col-md-4" key={food.id}>
        <div className="card mb-4 shadow-sm">
          <Link to={`recipe?food=${food.id}`}>
            <img className="Cuisine-home-item-img" src={food.image} alt={food.name} />
          </Link>
          <div className="card-body">
            <div className="d-flex justify-content-between align-items-center">
              <div className="btn-group">
                <a href={food.market? (food.market): "#"} target={"_blank"}>
                  <button type="button" className="btn btn-sm btn-outline-secondary" id="btn-font">맛집 링크</button>
                </a>
                <Link to={`search_cuisine?search=${food.name}`}>
                  <button type="button" className="btn btn-sm btn-outline-secondary" id="btn-font">관련 검색</button>
                </Link>
              </div>
              <p className="text-muted">{food.name}</p>
            </div>
          </div>
        </div>
      </div>
    )
  });

  


  return isLoading ? (
    <section className="App-section-layout4">
      <Spinner type="grow" color="primary" />
      <Spinner type="grow" color="secondary" />
      <Spinner type="grow" color="success" />
      <Spinner type="grow" color="danger" />
      <Spinner type="grow" color="warning" />
      <Spinner type="grow" color="info" />
      <Spinner type="grow" color="dark" />

      <h4 style={{ color: 'main-color' }}><br />추천음식이 준비되고 있습니다!</h4><br />
      <p className="Cuisine-home-loading-p">잠시만 기다려주세요</p>
    </section> //Loading Render.
  ) : (
    <section className="jumbotron text-center ">
      <div className="container">
        <h2 className="jumbotron-heading main-color">오늘의 추천 메뉴</h2>
        <p className="blue Cuisine-recommend-title">오늘 뭐 먹지에서 추천하는 금일의 메뉴</p>
        <p className="logoblue">그림을 클릭하면 레시피를 볼 수 있습니다.</p>
      </div>
      <div className="container">
        <div className="row">
          {food_list}
        </div>
      </div>

      <small className="text-muted">맘에 드는 메뉴가 없나요?</small>
      <p>
        <Link to="/worldcup">
          <button className="btn btn-secondary my-2">재설정하기</button>
        </Link>
      </p>

    </section>
  );
};

export default Cuisine_recommend_home;
