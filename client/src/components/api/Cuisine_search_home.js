import React, { useState, useEffect } from 'react';
import { Pagination, PaginationItem, PaginationLink} from 'reactstrap';
import '../../style/Api.css';
import axios from 'axios';
import { Link, useHistory} from 'react-router-dom';
const {REACT_APP_IP} = process.env;


const Cuisine_home = () => {
  const [food_data, setFood_date] = useState([]); //음식 데이터
  const [page, setPage]= useState(0); // n번 클릭시 이동

  let isSearch = document.location.href.split('?')[1];
  
  const [food_size, setFood_Size]= useState(0); //음식 갯수
  let [array, setArray]= useState([]); //pagination size
  const history= useHistory();


  useEffect(() => {

      axios.get(`http://${REACT_APP_IP}:8000/api/v1/food/food_count?${isSearch}`)
      .then(response => {
        
        if(response.data[0].size===0){
          alert("해당하는 음식이 존재하지 않습니다");
          return;
        }

        setFood_Size(Math.ceil(response.data[0].size/6)<5 ? Math.ceil(response.data[0].size/6): 5 );
        let temp=[];

        for(let i=0; i<food_size; i++){
          temp.push({id:`${i+1}`})
        }
        setArray(temp);
      }) 
      .catch(() => {});

      axios.get(`http://${REACT_APP_IP}:8000/api/v1/food/food_search?${isSearch}&page=${page}`)
        .then(response => {
          setFood_date(response.data);

        })
        .catch(() => {
          alert("서버가 불안정합니다. 잠시후 다시 시도 해주세요");
          history.push('/');
        });


  }, [isSearch, page, food_size, history]);

  

  const paginationClick= (e)=>{
    setPage(e.target.value-1);
  }

  const resetPagination=()=>{
    setPage(0);
  }


  const food_list= food_data.map(food => {

    return (
      <div className="col-md-4" key={food.id}>
        <div className="card mb-4 shadow-sm">
          <Link to={`recipe?food=${food.id}`}>
            <img className="Cuisine-home-item-img" src={food.image} alt={food.name} />
          </Link>
          <div className="card-body">
            <div className="d-flex justify-content-between align-items-center">
              <div className="btn-group">
                <a href={food.market? (food.market): "#"} target={"_blank"}>
                  <button type="button" className="btn btn-sm btn-outline-secondary" id="btn-font">맛집 링크</button>
                </a>
                <Link to={`search_cuisine?search=${food.name}`} onClick={resetPagination}>
                  <button type="button" className="btn btn-sm btn-outline-secondary" id="btn-font">관련 검색</button>
                </Link>
              </div>
              <p className="text-muted">{food.name}</p>
            </div>
          </div>
        </div>
      </div>
    )
  });


  return (
  <section className="jumbotron text-center ">
    <h4 className="search-title">그림을 클릭하면 레시피를 볼 수 있습니다.</h4>
    <div className="container">
      <div className="row">
        {food_list}
      </div>
    </div>
    
    <div className="Cuisine_home_pagination">
      <Pagination aria-label="Page navigation example">
        <PaginationItem disabled>
          <PaginationLink first href="#" />
        </PaginationItem>
        <PaginationItem disabled>
          <PaginationLink previous href="#" />
        </PaginationItem>
        
        {array.map((page)=>{
          return(
            <PaginationItem key={page.id}>
              <PaginationLink value={page.id} onClick={paginationClick}>
                {`${page.id}`}
              </PaginationLink>
            </PaginationItem>
          );
        })}

        <PaginationItem>
          <PaginationLink next href="#" />
        </PaginationItem>
        <PaginationItem>
          <PaginationLink last href="#" />
        </PaginationItem>
      </Pagination>
    </div>
  </section>
  );
};

export default Cuisine_home;


