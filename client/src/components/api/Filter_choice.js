import React from 'react'
import { useHistory} from 'react-router-dom';
import { Button } from 'reactstrap';

import branch from '../../images/food-icon/branch1.jpg';
import crab from '../../images/food-icon/crab.png';
import cucumber from '../../images/food-icon/cucumber1.jpg';
import egg from '../../images/food-icon/egg.png';
import fish from '../../images/food-icon/fish1.jpg';
import milk from '../../images/food-icon/milk1.jpg';
import peanut from '../../images/food-icon/peanut.jpg';
import rawfish from '../../images/food-icon/rawfish.png';
import cram from '../../images/food-icon/cram.png';


const Filter_choice = (props) => {

    const history = useHistory();

    const include= props.location.state.include;
    let exclude= [];
    
    const eachBtnClick= (event)=>{
        
        if(exclude.includes(event.target.alt)){
            event.target.style.opacity=1;

            for(let i=0; i<exclude.length; i++){
                if(exclude[i]=== event.target.alt){
                    exclude.splice(i,1);
                }
            }
        }
        
        else{
            exclude.push(event.target.alt);
            event.target.style.opacity=0.2;
        }
    }

    const submitBtnClick= ()=>{
        history.push({
            pathname:'/recommend_home',
            state:{include:include, exclude: exclude}
        })
    }
    
    return (
        <section className="jumbotron text-center" id="filter">
            <p className="section-title">싫어하는 재료를 선택해주세요</p>
            <p>(선택사항)</p>
            <div className="row">
                <div className="col-md-4 filter">
                    <div className="card-body">
                        <img className="Cuisine-filter-img" onClick={eachBtnClick} src={milk} alt="유제품" />
                    </div>
                    <p>유제품</p>
                </div>

                <div className="col-md-4 filter">
                    <div className="card-body">
                        <img className="Cuisine-filter-img" onClick={eachBtnClick} src={branch} alt="가지" />
                    </div>
                        <p>가지</p>  
                </div>

                <div className="col-md-4 filter">
                    <div className="card-body">
                        <img className="Cuisine-filter-img" onClick={eachBtnClick} src={crab} alt="갑각류" />
                    </div>
                        <p>갑각류</p>
                </div>

                <div className="col-md-4 filter">
                    <div className="card-body">
                        <img className="Cuisine-filter-img" onClick={eachBtnClick} src={cucumber} alt="오이" />
                    </div>
                        <p>오이</p>   
                </div>

                <div className="col-md-4 filter">
                    <div className="card-body">
                        <img className="Cuisine-filter-img" onClick={eachBtnClick} src={egg} alt="계란" />
                    </div>
                        <p>계란</p>
                </div>

                <div className="col-md-4 filter">
                    <div className="card-body">
                        <img className="Cuisine-filter-img" onClick={eachBtnClick} src={fish} alt ="생선" />
                    </div>
                    <p>생선</p>
                </div>

                <div className="col-md-4 filter">
                    <div className="card-body">
                        <img className="Cuisine-filter-img" onClick={eachBtnClick} src={peanut} alt="견과류" />
                    </div>
                    <p>견과류</p>
                </div>

                <div className="col-md-4 filter">
                    <div className="card-body">
                        <img className="Cuisine-filter-img" onClick={eachBtnClick} src={rawfish} alt ="회" />  
                    </div>
                    <p>회</p> 
                </div> 

                <div className="col-md-4 filter">
                    <div className="card-body">
                        <img className="Cuisine-filter-img" onClick={eachBtnClick} src={cram} alt ="조개류" />  
                    </div>
                    <p>조개류</p> 
                </div> 
                
                <Button className="Cuisine-filter-submitBtn" onClick={submitBtnClick}>선택 완료</Button>
            </div>
         </section>
    )
}

export default Filter_choice;