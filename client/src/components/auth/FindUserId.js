import React, { useState } from 'react';
import { Breadcrumb, BreadcrumbItem, Button, InputGroup, Input } from 'reactstrap';
import axios from 'axios';
import { Link } from 'react-router-dom';
const {REACT_APP_IP} = process.env;


const inputStyle={
  width:'100%',
  marginTop:'3%'
}

const itemStyle = {
  marginRight: '30px',
  fontSize: '0.8rem',
};

const FindUserId = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');

  const nameInputChange = event => {
    setName(event.target.value);
  };

  const emailInputChange = event => {
    setEmail(event.target.value);
  };

  const findIDKeyPress = event => {
    if (event.key === 'Enter') {
      findBtnClicked();
    }
  };

  const findBtnClicked = _ => {
    if (name.trim() === '' || name === undefined) {
      alert('이름을 입력해주세요');
      return;
    }
    if (email.trim() === '' || email === undefined) {
      alert('이메일을 입력해주세요');
      return;
    }
    axios.post(`http://${REACT_APP_IP}:8000/api/v1/auth/finduser`, { name: name, email: email })
      .then(response => {
        alert(`아이디는: ${response.data} 입니다.`);
      })
      .catch(()=> {
        alert('일치하는 정보가 존재하지 않습니다');
      });
  };

  return (
    <section className="Auth-section-layout">
      <h3>아이디 찾기</h3>

      <InputGroup style={inputStyle}>
        <Input type="text" value={name} name="name" placeholder="이름" maxLength="10" onChange={nameInputChange} onKeyPress={findIDKeyPress} />
      </InputGroup>

      <InputGroup style={inputStyle}>
        <Input type="text" value={email} name="email" placeholder="이메일" maxLength="40" onChange={emailInputChange} onKeyPress={findIDKeyPress}/>
      </InputGroup>

      <Button style={inputStyle} onClick={findBtnClicked} color="secondary" size="lg">찾기</Button>
      <hr />

      <Breadcrumb style={inputStyle} tag="nav" listTag="div">
        <Link to="/login">
          <BreadcrumbItem style={itemStyle}>로그인</BreadcrumbItem>
        </Link>
        <Link to="/signup">
          <BreadcrumbItem style={itemStyle}>회원가입</BreadcrumbItem>
        </Link>
      </Breadcrumb>
    </section>
  );
};

export default FindUserId;
