import React, { useState } from 'react';
import { Breadcrumb, BreadcrumbItem, Button, InputGroup, Input,} from 'reactstrap';
import { Link } from 'react-router-dom';


const inputStyle={
  width:'100%',
  marginTop:'3%'
}


const FindUserPw = () => {

  const [id, setId] = useState('');
  const [email, setEmail] = useState('');

  const idInputChange = event => {
    setId(event.target.value);
  };

  const emailInputChange = event => {
    setEmail(event.target.value);
  };

  const findPwKeyPress = event => {
    if (event.key === 'Enter') {
      findPwClicked();
    }
  };

  const findPwClicked = _ => {
    if (id.trim() === '' || id === undefined) {
      alert('아이디를 입력해주세요');
      return;
    }
    if (email.trim() === '' || email === undefined) {
      alert('이메일을 입력해주세요');
      return;
    }
    //서버와 연결
    /**
     *  <개선해야 할점>
     *  비밀번호 찾기
     *  비밀번호 재설정
     */
  };


  return (
    <section className="Auth-section-layout">
      <h3>비밀번호 찾기</h3>

      <InputGroup style={inputStyle}>
        <Input type="text" value={id} name="id" maxLength="20" placeholder="아이디" onChange={idInputChange} />
      </InputGroup>

      <InputGroup style={inputStyle}>
        <Input type="text" value={email} name="email" maxLength="40" placeholder="이메일" onChange={emailInputChange} onKeyPress={findPwKeyPress} />
      </InputGroup>

      <Button style={inputStyle} onClick={findPwClicked} color="secondary" size="lg">찾기</Button>
      <hr />

      <Breadcrumb style={inputStyle} tag="nav" listTag="div">
        아이디를 찾으시나요?&nbsp;
        <Link to="/findUserId">
          <BreadcrumbItem>아이디 찾기</BreadcrumbItem>
        </Link>
      </Breadcrumb>
    </section>
  );
};

export default FindUserPw;
