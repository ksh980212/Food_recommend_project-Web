import React, { useState } from 'react';
import { Breadcrumb, BreadcrumbItem, Button, InputGroup, Input } from 'reactstrap';
import axios from 'axios';
import { Link, useHistory } from 'react-router-dom';

const {REACT_APP_IP} = process.env;


const itemStyle = {
  marginRight: '30px',
  fontSize: '0.8rem',
};

const loginInputStyle={
  width:'100%',
  marginTop:'3%'
}



const Login = props => {
  let history = useHistory();

  const [id, setId] = useState('');
  const [password, setPassword] = useState('');

  const idInputChange = event => {
    setId(event.target.value);
  };
  const passwordInputChange = event => {
    setPassword(event.target.value);
  };

  const loginKeyPress = event => {
    if (event.key === 'Enter') {
      loginBtnClicked();
    }
  };

  const loginBtnClicked = event => {
    if (id.trim() === '' || id === undefined) {
      alert('아이디를 입력해주세요');
      return;
    }
    if (password.trim() === '' || password === undefined) {
      alert('비밀번호를 입력해주세요');
      return;
    }

    axios.post(`http://${REACT_APP_IP}:8000/api/v1/auth/login`, { user_id: id, password: password })
      .then(response => {
        if (response.data.token) {
          history.push('/');

          /**
           * <개선할점>
           *  signup, signupComplete 가 이전 페이지면  history.push('/')
           *  나머지는 history.back()
           */
           
          localStorage.setItem('token', response.data.token);
          localStorage.setItem('user_id', response.data.user_id);
          localStorage.setItem("instagram", response.data.instagram);

          props.changeLoginState();

          alert('로그인 되었습니다');
        }
      })
      .catch(() => {
        alert('로그인에 실패하셨습니다');
      });
  };
  
  return (
    <section className="Auth-section-layout">
      <h3 className="center">로그인</h3>
      
      <React.Fragment>
        <InputGroup style={loginInputStyle}>
          <Input type="text" value={id} maxLength="20" name="id" placeholder="아이디" onChange={idInputChange} onKeyPress={loginKeyPress} />
        </InputGroup>

        <InputGroup style={loginInputStyle}>
          <Input type="password" value={password} maxLength="20" name="password" placeholder="비밀번호" onChange={passwordInputChange} onKeyPress={loginKeyPress}/>
        </InputGroup>

        <Button style={loginInputStyle} onClick={loginBtnClicked} color="secondary" size="lg">로그인</Button>
        <hr />

        <Breadcrumb style={loginInputStyle} tag="nav" listTag="div">
          <Link to="/findUserId">
            <BreadcrumbItem style={itemStyle}>아이디 찾기</BreadcrumbItem>
          </Link>
          <Link to="/signup">
            <BreadcrumbItem style={itemStyle}>회원가입</BreadcrumbItem>
          </Link>
        </Breadcrumb>
        <small className="mt-5 mb-3 text-muted inital center">&copy; 오늘 뭐 먹지?</small>
      </React.Fragment>
    </section>
  );
};
export default Login;
