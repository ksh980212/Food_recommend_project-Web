import React, {useState} from 'react'
import { Button, InputGroup, InputGroupAddon, Input } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
const {REACT_APP_IP} = process.env;


const signupBtnStyle={
    width:'100%',
    marginTop:'3%'
  }


const Privacy = () => {

    const history = useHistory();
    const [id] = useState(localStorage.getItem('user_id'));
    const [isLoad, setIsLoad]= useState(false);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const[instagramID, setInstagramID]= useState('');
    const [priorName, setPriorName]= useState('');
    const [priorEmail, setPriorEmail]  =useState('');
    const [priorInstagramID, setPriorInstagram] = useState('');

    
    if(localStorage.getItem('token')===null){
        history.push('/');
    }
    
    const nameInputChange = event => {
      setName(event.target.value);
    };
  
    const emailInputChange = event => {
      setEmail(event.target.value);
    };
    
    const instagramInputChange = event=>{
        setInstagramID(event.target.value);
    }

    const signupKeyPress = (event)=>{
        if(event.key==="Enter"){
            updateData();
        }
    }

    const loadData = ()=>{
        axios.post(`http://${REACT_APP_IP}:8000/api/v1/auth/getUserInfo`, {
            user_id: id
        }).then((response)=>{
            const data= response.data;
            setName(data.name);
            setEmail(data.email);

            if(data.instargramID!==undefined){
                setInstagramID(data.instargramID);
                setPriorInstagram(data.instargramID);
            }
            else{
                setInstagramID('');
                setPriorInstagram('');
            }

            setPriorEmail(data.email);
            setPriorName(data.name);

            setIsLoad(true);
        }).catch(()=>{
            alert('불러오기에 실패하였습니다');
        })
    }
    
    const updateData =()=>{
        if(isLoad===false){
            alert("불러오기 후 가능합니다");
            return;
        }

        if(priorName=== name && priorEmail === email && priorInstagramID === instagramID){
            alert("변경후 수정가능합니다");
            return;
        }

        if(name=== undefined|| name.trim()==="" || email === undefined || email.trim() === ""){
            alert("이름과 이메일을 입력해주세요");
            return;
        }

        if(window.confirm("정말로 수정하시겠습니까?")){
            axios.put(`http://${REACT_APP_IP}:8000/api/v1/auth/updateUserInfo`, {
                user_id: id,
                name: name,
                email:email,
                instagramID:instagramID
            }).then(()=>{
                history.push("/");
                localStorage.setItem('instagram', instagramID);
                alert("수정되었습니다");
            }).catch(()=>{
                alert('수정이 실패하였습니다');
            })
        }
    }

    
    return (
        <section className="Auth-section-layout">
        <h3 className="center">회원정보변경</h3>

        <InputGroup style={signupBtnStyle}>
          <InputGroupAddon addonType="prepend">이름</InputGroupAddon>
          <Input type="text" name="name" maxLength="10" value={name} placeholder="불러오기 해주세요!" onChange={nameInputChange} />
        </InputGroup>
  
        <InputGroup style={signupBtnStyle}>
          <InputGroupAddon addonType="prepend">이메일</InputGroupAddon>
          <Input type="text" value={email} maxLength="30" name="email" placeholder="" onChange={emailInputChange} />
        </InputGroup>

        <InputGroup style={signupBtnStyle}>
          <InputGroupAddon addonType="prepend">인스타 아이디</InputGroupAddon>
          <Input type="text" onKeyPress={signupKeyPress} placeholder="" value={instagramID} maxLength="30" name="email" onChange={instagramInputChange} />
        </InputGroup>
  
        <hr />
        <p>불러오기 후 수정 가능합니다 ▼</p>
        <Button style={signupBtnStyle} onClick={loadData} color="secondary" size="lg">불러오기</Button>
        <Button style={signupBtnStyle} onClick={updateData} color="secondary" size="lg">수정하기</Button>
        <small className="mt-5 mb-3 text-muted inital center">&copy; 오늘 뭐 먹지?</small>
      </section>
    )
}

export default Privacy;