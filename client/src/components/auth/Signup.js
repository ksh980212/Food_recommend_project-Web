import React, { useState } from 'react';
import { Button, InputGroup, Input } from 'reactstrap';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
const {REACT_APP_IP} = process.env;


const signupBtnStyle={
  width:'100%',
  marginTop:'3%',
  color:'black'
}


const Signup = () => {
  let history = useHistory();

  const [id, setId] = useState('');
  const [password, setPassword] = useState('');
  const [password_confirm, setPassword_confirm] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');

  const idInputChange = event => {
    setId(event.target.value);
  };

  const passwordInputChange = event => {
    setPassword(event.target.value);
  };

  const password_confirmInputChange = event => {
    setPassword_confirm(event.target.value);
  };

  const nameInputChange = event => {
    setName(event.target.value);
  };

  const emailInputChange = event => {
    setEmail(event.target.value);
  };

  const signupKeyPress = event => {
    if (event.key === 'Enter') {
      signupBtnClicked();
    }
  };

  const signupBtnClicked = _ => {
    if (id.trim() === '' || id === undefined) {
      alert('아이디를 입력해주세요');
      return;
    }

    if ( password.trim() === '' || password === undefined || password_confirm.trim() === '' || password_confirm === undefined) {
      alert('비밀번호를 입력해주세요');
      return;
    }

    if (password.length < 8) {
      alert('비밀번호는 최소 8자리 이상 해주세요');
      return;
    }

    if (password !== password_confirm) {
      alert('비밀번호가 일치하지 않습니다');
      return;
    }

    if (name.trim() === '' || name === undefined) {
      alert('이름을 입력해주세요');
      return;
    }

    if (email.trim() === '' || email === undefined) {
      alert('이메일을 입력해주세요');
      return;
    }
    if(email.includes(['@'])===false){
      alert("이메일 형식이 아닙니다");
      return;
    }

    if(window.confirm("정말로 회원가입 하시겠습니까?")){
      axios.post(`http://${REACT_APP_IP}:8000/api/v1/auth/signup`, { user_id: id, name: name, email: email, password: password })
        .then(_ => {
          history.push('/signupComplete');
          alert("가입되었습니다");
        })
        .catch(() => {
          alert('이미 존재하는 계정입니다');
        });
    }
  };

  return (
    <section className="Auth-section-layout">
      <h3 className="center">회원가입</h3>

      <InputGroup style={signupBtnStyle}>
        <Input type="text" maxLength="20" value={id} name="id" placeholder="아이디" onChange={idInputChange} onKeyPress={signupKeyPress}/>
      </InputGroup>

      <InputGroup style={signupBtnStyle}>
        <Input type="password" value={password} name="password" maxLength="20" placeholder="비밀번호 (8자 이상 필수)" onChange={passwordInputChange} onKeyPress={signupKeyPress} />
      </InputGroup>
      <InputGroup style={signupBtnStyle}>
        <Input type="password" value={password_confirm} name="password_confirm" maxLength="20" placeholder="비밀번호 확인 (8자 이상 필수)" onChange={password_confirmInputChange} onKeyPress={signupKeyPress}/>
      </InputGroup>

      <InputGroup style={signupBtnStyle}>
        <Input type="text" name="name" maxLength="10" value={name} placeholder="이름" onChange={nameInputChange} onKeyPress={signupKeyPress}/>
      </InputGroup>

      <InputGroup style={signupBtnStyle}>
        <Input type="text" value={email} maxLength="30" name="email" placeholder="이메일" onChange={emailInputChange} onKeyPress={signupKeyPress}/>
      </InputGroup>
      
      <hr />

      <Button style={signupBtnStyle} onClick={signupBtnClicked} color="secondary" size="lg">가입하기</Button>
      <small className="mt-5 mb-3 text-muted inital center">&copy; 오늘 뭐 먹지?</small>
    </section>
  );
};

export default Signup;
