import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';


const style = {
  width: '50%',
};


const SignupComplete = () => {
  return (
    <section className="App-section-layout2">
      <h3 className="center">회원가입이 완료되었습니다</h3>
      <hr />

      <div className="Auth-signupcomplete-layout">
        <Link to="/login">
          <Button style={style} color="secondary" className="login">
            로그인하기
          </Button>
        </Link>
      </div>
      <small className="mt-5 mb-3 text-muted inital inital center">&copy; 오늘 뭐 먹지?</small>
    </section>
  );
};

export default SignupComplete;
