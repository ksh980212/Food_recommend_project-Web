import { createGlobalStyle } from "styled-components";
import reset from "styled-reset";


const resetStyle = createGlobalStyle`
${reset}
a{
    text-decoration:none;
    color:inherit;
}
*{
    box-sizing:boerder-box;
}
body{
    font-size: 14px;
    background-color:rgba(20,20,20,1);
}
`;

export default resetStyle;