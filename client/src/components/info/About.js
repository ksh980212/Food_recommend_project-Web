import React from 'react';
import mail from '../../images/mail.jpg';
import '../../util/color.css';


const About = () => {
  return (
    <section className="App-section-layout3">
      <h3><b>오늘 뭐 먹지?</b></h3>
      <hr />

      <p className="green">오늘의 메뉴와 레시피를 추천해드립니다.</p>
      <p className="blue">당신의 취향에 맞는 음식으로!</p>
      <hr />

      <h4>인스타추천 방법</h4>
      <br />

      <p className="blue">1. <u>로그인</u>을 한다.(처음 사용시 회원가입)</p>

      <p className="green">2. <u>인스타추천</u> 버튼을 누른다.</p>

      <p className="green">3. 추천된 6가지 음식을 둘러본다.</p>

      <p className="blue">4. <u>레시피</u>를 보며 만든다!</p>
      
      <hr />

      <h4>일반추천 방법</h4>
      <br />

      <p className="blue">1. <u>로그인</u>을 한다.(처음 사용시 회원가입)</p>

      <p className="green">2. <u>일반추천</u> 버튼을 누른다.</p>

      <p className="blue">3. 원하는 음식을 3가지 클릭한다.</p>

      <p className="green">4. 싫어하는 재료가 있으면 선택한다.</p>

      <p className="blue">5. 추천된 6가지 음식을 둘러본다.</p>

      <p className="green">6. <u>레시피</u>를 보며 만든다!</p>
      
      <hr />

      <h4>음식 검색 방법</h4>
      <br />

      <p className="blue">1. <u>로그인</u>을 한다.(처음 사용시 회원가입)</p>

      <p className="green">2. <u>(데스크톱 이용시)</u> 오른쪽 상단의 검색하기를 이용한다.</p>

      <p className="blue">3.  <u>(모바일 이용시)</u> 오른쪽 상단의 버튼을 클릭 후 검색하기를 이용한다.</p>

      <p className="green">4. 검색된 음식을 둘러본다.</p>

      <p className="blue">5. <u>레시피</u>를 보며 만든다!</p>
      
      <hr />

      <a href="mailto:ksh980212@gmail.com">
        <img src={mail} className="mail-img" alt="mail" />
        <p className="comment">문의하기</p>
      </a>

      <p className="comment">(언제나 환영합니다.)</p>
      <hr />
      <small className="mt-5 mb-3 text-muted inital center">&copy; 오늘 뭐 먹지?</small>
      
    </section>
  );
};

export default About;
