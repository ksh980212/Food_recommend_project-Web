import React, { useState } from 'react';
import { Table } from 'reactstrap';
import '../../style/Info.css';


const Developer = () => {
  const [developer] = useState([
    { id: 1,  name: '김서아', email: '01071640671@naver.com', charge:"딥러닝으로 AI 모델 담당" },
    { id: 2,  name: '류경아', email: "thedreamproject@naver.com" ,  charge:"딥러닝으로 AI 모델 담당"},
    { id: 3,  name: '김현광', email: 'khkhy8712@naver.com' ,  charge:"데이터 수집/전처리 담당"},
    { id: 4,  name: '이승완', email: 'toexe@naver.com',  charge:"데이터 수집/전처리 담당"},
    { id: 5,  name: '최형진', email: 'chjin1122@naver.com', charge:"데이터 크롤링 담당"},
    { id: 6,  name: '강승호', email: 'ksh980212@gmail.com', charge:"웹 서비스/인프라 담당"}
  ]);

  return (
    <section className="Dev-table-layout">
      <h3><b>Food Tech team</b></h3>
      <br />

      <Table responsive striped>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Task</th>
          </tr>
        </thead>
        
        <tbody>
          {developer.map(dev => {
            return (
              <tr key={dev.id}>
                <th scope="row">{dev.id}</th>
                <td>{dev.name ? dev.name : '-'}</td>
                <td>{dev.email ? dev.email : '-'}</td>
                <td>{dev.charge? dev.charge : '-'}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>

      <small className="mt-5 mb-3 text-muted inital center">&copy; 오늘 뭐 먹지?</small>
      
    </section>
  );
};

export default Developer;
