import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Progress } from 'reactstrap';
import axios from 'axios';
import '../../style/Info.css';

const {REACT_APP_IP} = process.env;


const Evaluation = () => {
  const [review, setReview] = useState(3);
  const [count, setCount] = useState(0);
  const history= useHistory();

  useEffect(() => {
    axios.get(`http://${REACT_APP_IP}:8000/api/v1/food/food_review`)
      .then(response => {
        setReview(Number(response.data[0].average).toFixed(1));
        setCount(Number(response.data[0].count));
      })
      .catch(() => {
        alert("서버가 불안정합니다. 잠시후 다시 시도 해주세요");
        history.push('/');
      });
  }, [history]);
  
  return (
    <section className="App-section-layout2">
      <h2 className="main-color">사용자 평가 지표</h2>
      <div className="Evaluation-div-layout">
        <p className="black">{review}/5점</p>
      </div>

      <Progress value={review * 20} />

      <div className="Evaluation-div-layout">
        <h5 className="black">현재까지 {count}명의 사람이 평가하였습니다.</h5>
      </div>
      
      <small className="mt-5 mb-3 text-muted inital center">&copy; 오늘 뭐 먹지?</small>
    </section>
  );
};

export default Evaluation;
