import React, {useState, useEffect} from 'react';
import { Jumbotron, Button,  Carousel, CarouselItem, CarouselControl, CarouselIndicators } from 'reactstrap';
import { Link, useHistory } from 'react-router-dom';
import { Progress } from 'reactstrap';
import axios from 'axios';
import '../../style/App.css';

import mail from '../../images/mail.jpg';
import mainlogo from '../../images/mainlogo.jpg'
import phone from '../../images/phone.jpg'
import main1 from '../../images/main1.jpg'
import main2 from '../../images/main2.jpg'


const {REACT_APP_IP} = process.env;


const buttonStyle={
  width:'10%',
  minWidth:'140px',
  margin:'2%'
}

const items = [


  {
    src:main1
  },
  {
    src:main2
  },
  {
    src:mainlogo
  }
];


const Home = ({isLogined}) => {
  
  const history=useHistory();
  const [review, setReview] = useState(3);
  const [count, setCount] = useState(0);

  useEffect(() => {
    axios.get(`http://${REACT_APP_IP}:8000/api/v1/food/food_review`)
      .then(response => {
        setReview(Number(response.data[0].average).toFixed(1));
        setCount(Number(response.data[0].count));
      })
      .catch(() => {
        alert("서버가 불안정합니다. 잠시후 다시 시도 해주세요");
        history.push('/');
      });
  }, [history]);

  const [instagram, setInstagram] = useState('');

  useEffect(()=>{
    if(localStorage.getItem('instagram')){
      setInstagram(localStorage.getItem('instagram'));
    }
  },[instagram])

  
  const requestLogin =()=>{
    alert("로그인 후 이용 가능합니다");
    history.push("/login");
  }

  const noInstagram= ()=>{
    alert("인스타 계정을 등록해주세요");
    history.push("/privacy");
  }

  const toInstagram= ()=>{
    history.push({
      pathname:'/instagramRecommend',
      state:{instagram: instagram}
    })
  }
  
  //** Carousel  Setting */

  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img src={item.src} alt={item.altText} className="main-image" />
      </CarouselItem>
    );
  });


  return (
    <section className="App-section-layout9">
        <Carousel activeIndex={activeIndex} next={next} previous={previous}>
          <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
            {slides}
          <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
          <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
      </Carousel>

      <Jumbotron>
        <p className="normal">매일 매일 뭐 먹을지 고민될 때 </p>
        <hr />
        <h2 className="main-color">오늘 뭐 먹지?</h2>
        <hr />
        <p>당신이 좋아할 만한 음식과 레시피를 추천 해드립니다.</p>

        {
          isLogined? 
          (
            instagram!=='null' && instagram!=='undefined' && instagram.toString().trim()!==''?
                <p>
                  <Button style={buttonStyle} onClick={toInstagram} color="primary">인스타 계정 추천</Button>

                  <Link to="/worldcup">
                    <Button style={buttonStyle} color="info">일반 추천</Button>
                  </Link>
                </p>
              :
                <p>
                  <Button onClick={noInstagram} style={buttonStyle} color="primary">인스타 계정 추천</Button>

                <Link to="/worldcup">
                  <Button style={buttonStyle} color="info">일반 추천</Button>
                </Link>
              </p>
          )
        :
          <p>
            <Button style={buttonStyle} onClick={requestLogin} color="primary">인스타 계정 추천</Button>
              <Link to="/worldcup">
                    <Button style={buttonStyle} color="info">일반 추천</Button>
              </Link>
          </p>
        } 
        
        <small className="normal2 SCDream">상세한 사용방법은 아래에 있는 사용 방법을 참고해주세요▼</small>
      </Jumbotron>

      <img src={phone} className="main-image" alt="back"/>
      <section className="App-section-layout3">

        <h4 className="method-title">인스타추천 방법</h4>
        <br />

        <p className="method-desc">1 > 상단의 계정 정보에서 인스타 계정을 수정</p>

        <p className="method-desc">2 > 인스타추천 클릭</p>

        <p className="method-desc">3 > 추천된 음식의 레시피&맛집확인</p>

        
        <hr />

        <h4 className="method-title">일반추천 방법</h4>
        <br />

        <p className="method-desc">1 > 일반추천 버튼 클릭</p>

        <p className="method-desc">2 > 원하는 음식을 3가지 클릭</p>

        <p className="method-desc">3 > 제외하고 싶은 재료가 있으면 클릭</p>

        <p className="method-desc">4 > 추천된 음식의 레시피&맛집확인</p>


        
        <hr />

        <h4 className="method-title">음식 검색 방법</h4>
        <br />

        <p className="method-desc">1 > 오른쪽 상단의 검색하기를 이용</p>
        <p className="method-desc">2 > 검색한 음식의 레시피&맛집확인</p>
        
        <hr />
      </section>
      
{/* 
      <img src={main7} className="main-image2" alt="back"/>
      <hr /> */}

      <section className="App-section-layout3">
        <h2 className="method-title">사용자 만족도</h2>
        <div className="Evaluation-div-layout">
          <p className="method-title">{review}/5점</p></div>
        <Progress value={review * 20}/>

        <div className="Evaluation-div-layout">
          <h5 className="method-title">현재까지 {count}명의 사람이 평가하였습니다.</h5>
          <br />
          <h5 className="method-desc">평가는 레시피 페이지에서 하실 수 있습니다.</h5>
        </div>
    </section>

      <a href="mailto:ksh980212@gmail.com">
        <img src={mail} className="mail-img" alt="mail" />
        <p className="comment">문의하기</p>
      </a>

      <hr />
      
      <small className="mt-5 mb-3 text-muted inital center">&copy; 오늘 뭐 먹지?</small>
    </section>
    
  );
};

export default Home;