import React, {useState}from 'react';
import { NavLink, Navbar, Nav, NavItem, Collapse, NavbarToggler } from 'reactstrap';
import { Link } from 'react-router-dom';


const BottomNavbar = () => {

  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);

  return (
    <nav>
      <Navbar dark fixed="bottom">
        <NavbarToggler onClick={toggleNavbar} className="mr-2" />
        <Collapse isOpen={!collapsed} navbar>
          <Nav navbar>
            <NavItem>
              <NavLink tag={Link} to="/developer" onClick={toggleNavbar}>
                <span className="small">Developer</span>
              </NavLink>
            </NavItem>
            <NavItem>
                <NavLink tag={Link} to="/evaluation" onClick={toggleNavbar}>
                <span className="small">Analysis</span>
                </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </nav>
  );
};

export default BottomNavbar;
