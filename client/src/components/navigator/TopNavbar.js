import React, { useState } from 'react';
import { Button, Collapse, Navbar, NavbarToggler, Nav, NavItem, NavLink, Input} from 'reactstrap';
import { Link, useHistory } from 'react-router-dom';


const TopNavbar = ({ isLogined, changeLogoutState }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [search, setSearch] = useState('');
  let history = useHistory();

  const toggle = () => setIsOpen(!isOpen);

  const logOut = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('user_id');
    localStorage.removeItem('instagram');
    
    alert('로그아웃 되었습니다!');
    changeLogoutState();
  };

  const searchOnkeyPress = event => {
    if (event.key === 'Enter') {
      searchBtnClick();
    }
  };
  const searchBtnClick= _=>{
    if (search === undefined || search.trim() === '') {
      alert('검색어를 입력해주세요!');
      return;
    }
    toggle();
    history.push(`search_cuisine?search=${search.trim()}`);
  }

  const searchOnchange = event => {
    setSearch(event.target.value);
  };

  return (
    <nav>
      <Navbar dark expand="md">
        <NavLink tag={Link} to="/">
          <span className="title">오늘 뭐 먹지?</span>
        </NavLink>

        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav navbar>
            {
            isLogined ? 
              (
              <NavItem>
                <NavLink tag={Link} to="/privacy" onClick={toggle}>계정 정보</NavLink>
              </NavItem>
              ): 
              (
              <NavItem>
                <NavLink tag={Link} to="/signup" onClick={toggle}>회원가입</NavLink>
              </NavItem>
              )
            }

            {
            isLogined ? 
              (
                <NavItem  onClick={toggle}>
                  <NavLink onClick={logOut}>로그아웃</NavLink>
                </NavItem>
              ) : 
              (
                <NavItem>
                  <NavLink tag={Link} to="/login" onClick={toggle} >로그인</NavLink>
                </NavItem>
              )
            }
 
          </Nav>

          <Nav className="ml-auto" navbar>
            <NavItem>
              <Input onChange={searchOnchange} onKeyPress={searchOnkeyPress} onClick={searchOnchange} placeholder="궁금한 레시피를 검색하세요"/>
            </NavItem>
            <NavItem>
              <Button color="info" onClick={searchBtnClick}>검색하기</Button>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </nav>
  );
};

export default TopNavbar;
