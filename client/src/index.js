import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import "bootstrap/dist/css/bootstrap.min.css";
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import './style/index.css'; 
import { BrowserRouter } from 'react-router-dom';
import dotenv from 'dotenv';
dotenv.config();

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>, document.getElementById("root")
    );
